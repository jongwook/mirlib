# MIRLIB Makefile

UNAME       := $(shell uname)

ifeq ($(UNAME), Linux)
CC          := g++
LD          := g++
AR          := ar
CARGS       := -O3 -std=c++0x 
LDARGS      := -O3 -Wno-cpp
endif

ifeq ($(UNAME), Darwin)
CC          := clang++
LD          := clang++
AR          := libtool
CARGS       := -O3 -arch x86_64 -std=c++0x
LDARGS      := -O3 -arch x86_64
endif

MODULES     := io math tfr util mir

SRC_ROOT    := src
BUILD_ROOT  := build/bin

SRC_DIR     := $(addprefix $(SRC_ROOT)/,$(MODULES))
TESTS_DIR   := tests

INCLUDE_DIR := include
LIB_DIR     := lib

BUILD_DIR   := $(addprefix $(BUILD_ROOT)/,$(MODULES) $(TESTS_DIR))

SRC         := $(foreach sdir,$(SRC_DIR),$(wildcard $(sdir)/*.cpp))
TESTS_SRC   := $(wildcard $(TESTS_DIR)/*.cpp)

OBJ         := $(patsubst $(SRC_ROOT)/%.cpp,$(BUILD_ROOT)/%.o,$(SRC))
TESTS_OBJ   := $(patsubst $(TESTS_DIR)/%.cpp,$(BUILD_ROOT)/$(TESTS_DIR)/%.o,$(TESTS_SRC))

INCLUDES    := $(addprefix -I,$(SRC_ROOT) $(INCLUDE_DIR))

MIRLIB      := $(BUILD_ROOT)/libmirlib.a
MIRLIB_TEST := $(BUILD_ROOT)/mirlib-test

ifeq ($(UNAME), Linux)
GTEST_LIB   := -L$(LIB_DIR) -lgtest
endif
ifeq ($(UNAME), Darwin)
GTEST_LIB   := $(LIB_DIR)/libgtest.osx.x64.a
endif

TESTS_LIBS  := $(GTEST_LIB) -L$(BUILD_ROOT) -lmirlib -lpthread

vpath %.cpp $(SRC_DIR) $(TESTS_DIR)

.PHONY: all checkdirs clean

all: checkdirs mirlib mirlib-test

mirlib: $(MIRLIB)

mirlib-test: $(MIRLIB_TEST)

ifeq ($(UNAME), Linux)
$(MIRLIB): $(OBJ)
	$(AR) rvs $@ $^
endif
ifeq ($(UNAME), Darwin)
$(MIRLIB): $(OBJ)
	$(AR) -static -arch_only x86_64 $^ -o $@ 
endif

$(MIRLIB_TEST): $(TESTS_OBJ)
	$(LD) $^ $(LDARGS) $(TESTS_LIBS) -o $@

checkdirs: $(BUILD_DIR)

$(BUILD_DIR):
	mkdir -p $@

clean : 
	rm -rf $(BUILD_ROOT)/*

define make-goal
$1/%.o: %.cpp
	$(CC) $(CARGS) $(INCLUDES) -c $$< -o $$@
endef

$(foreach bdir,$(BUILD_DIR),$(eval $(call make-goal,$(bdir))))

