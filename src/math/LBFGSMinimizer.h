
#ifndef __LBFGSMinimizer_h__
#define __LBFGSMinimizer_h__

#include <list>

#include "GradientMinimizer.h"

class LBFGSMinimizer:public GradientMinimizer {
public:
	LBFGSMinimizer(int _maxIterations = 20);
	virtual ~LBFGSMinimizer();

	class IterationCallbackFunction {
	public:
		virtual void iterationDone(double *curGuess, int iter)=0;
	};

	void setMinIterations(int _minIterations) { minIterations = _minIterations; }
	void setMaxIterations(int _maxIterations) { maxIterations = _maxIterations; }
	void setInitialStepSizeMultiplier(double _initialStepSizeMultiplier) { initialStepSizeMultiplier = _initialStepSizeMultiplier; }
	void setStepSizeMultiplier(double _stepSizeMultiplier) { stepSizeMultiplier = _stepSizeMultiplier; }
	int minimize(DifferentiableFunction &function, double *initial, double tolerance, double *result) { return minimize(function, initial, tolerance, result, true); }
	int minimize(DifferentiableFunction &function, double *initial, double tolerance, double *result, bool printProgress);
	void setMaxHistorySize(int _maxHistorySize) { maxHistorySize = _maxHistorySize; }
	void setIterationCallbackFunction(IterationCallbackFunction &callbackFunction) { iterCallbackFunction = &callbackFunction; }

protected:
	bool converged(double value, double nextValue, double tolerance);
	void updateHistories(double *guess, double *nextGuess, double *derivative, double *nextDerivative, int dimension);
	void pushOntoList(double *vector, std::list<double *> &vectorList);
	int historySize() { return (int)inputDifferenceVectorList.size(); }
	double* getInputDifference(int num);
	double *getDerivativeDifference(int num);
	double* getLastDerivativeDifference() { return derivativeDifferenceVectorList.front(); }
	double* getLastInputDifference() { return inputDifferenceVectorList.front(); }
	int implicitMultiply(double *initialInverseHessianDiagonal, double *derivative, double *result, int dimension);
	int getInitialInverseHessianDiagonal(DifferentiableFunction &function, double *result, int dimension);

	double EPS;
	int maxIterations;
	int maxHistorySize;
	std::list<double *> inputDifferenceVectorList;
	std::list<double *> derivativeDifferenceVectorList;
	IterationCallbackFunction *iterCallbackFunction;
	int minIterations;
	double initialStepSizeMultiplier;
	double stepSizeMultiplier;
};


#endif

