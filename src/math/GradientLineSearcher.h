
#ifndef __math_GradientLineSearcher_h__
#define __math_GradientLineSearcher_h__

#include "DifferentiableFunction.h"


class GradientLineSearcher {
public:
	virtual ~GradientLineSearcher() {}
	virtual int minimize(DifferentiableFunction &function, double *initial, double *direction, double *result)=0;
};


#endif 
