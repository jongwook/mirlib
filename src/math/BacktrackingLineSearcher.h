
#ifndef __math_BacktrackingLineSearcher_h__
#define __math_BacktrackingLineSearcher_h__

#include "GradientLineSearcher.h"



class BacktrackingLineSearcher:public GradientLineSearcher {
public:
	BacktrackingLineSearcher();
	virtual ~BacktrackingLineSearcher();

	int minimize(DifferentiableFunction &function, double *initial, double *direction, double *result);
	int minimize(DifferentiableFunction &function, double *initial, double *direction, double initialValue, double *initialDerivative, double *guess, double &guessedValue);

	double stepSizeMultiplier;
protected:
	double EPS;
	double sufficientDecreaseConstant;
};



#endif 