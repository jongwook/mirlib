
#include <cfloat>
#include <cstring>
#include <cmath>
#include <sstream>

#include "DoubleArrays.h"
#include "SloppyMath.h"



void DoubleArrays::assign(double *y, double *x, int length) {
	memcpy(y, x, length * sizeof(double));
}

double DoubleArrays::innerProduct(double *x, double *y, int length) {
	double result = 0.0;
	for(int i=0; i<length; i++) {
		result += x[i] * y[i];
	}
	return result;
}

void DoubleArrays::addMultiples(double *x, double xMultiplier, double *y, double yMultiplier, int length, double *result) {
	for(int i=0; i<length; i++) {
		result[i] = x[i] * xMultiplier + y[i] * yMultiplier;
	}
}

void DoubleArrays::constantArray(double c, int length, double *result) {
	for(int i=0; i<length; i++) {
		result[i] = c;
	}
}

void DoubleArrays::pointwiseMultiply(double *x, double *y, int length, double *result) {
	for(int i=0; i<length; i++) {
		result[i] = x[i] * y[i];
	}
}

std::string DoubleArrays::toString(double *x, int length) {
	std::stringstream ss;
	ss << "[";
	for(int i=0; i<length; i++) {
		ss << x[i];
		if(i+1 < length)
			ss << ", ";
	}
	ss << "]";
	return ss.str();
}

void DoubleArrays::scale(double *x, double s, int length) {
	if(s == 1.0)
		return;
	for(int i=0; i<length; i++) {
		x[i] *= s;
	}
}

void DoubleArrays::multiply(double *x, double s, int length, double *result) {
	if(s == 1.0) {
		memcpy(result, x, length*sizeof(double));
		return;
	}
	for(int i=0; i<length; i++) {
		result[i] = x[i] * s;
	}
}

int DoubleArrays::argMax(double *v, int length) {
	int maxI = -1;
	double maxV = -DBL_MAX;
	for(int i=0; i<length; i++) {
		if(v[i] > maxV) {
			maxV = v[i];
			maxI = i;
		}
	}
	return maxI;
}

double DoubleArrays::max(double *v, int length) {
	double maxV = -DBL_MAX;
	for(int i=0; i<length; i++) {
		if(v[i] > maxV) {
			maxV = v[i];
		}
	}
	return maxV;
}

int DoubleArrays::argMin(double *v, int length) {
	int minI = -1;
	double minV = DBL_MAX;
	for(int i=0; i<length; i++) {
		if(v[i] < minV) {
			minV = v[i];
			minI = i;
		}
	}
	return minI;
}

double DoubleArrays::min(double *v, int length) {
	double minV = DBL_MAX;
	for(int i=0; i<length; i++) {
		if(v[i] < minV) {
			minV = v[i];
		}
	}
	return minV;
}

double DoubleArrays::maxAbs(double *v, int length) {
	double maxV = 0;
	for(int i=0; i<length; i++) {
		double abs = (v[i] <= 0.0)? 0.0-v[i]: v[i];
		if(abs>maxV) {
			maxV = abs;
		}
	}
	return maxV;
}

void DoubleArrays::add(double *a, double b, int length, double *result) {
	for(int i=0; i<length; i++) {
		result[i] = a[i] + b;
	}
}

double DoubleArrays::add(double *a, int length) {
	double sum = 0.0;
	for(int i=0; i<length; i++) {
		sum += a[i];
	}
	return sum;
}

double DoubleArrays::add(double *a, int first, int last) {
	double sum = 0.0;
	for(int i=first; i<=last; i++) {
		sum += a[i];
	}
	return sum;
}

double DoubleArrays::vectorLength(double *x, int length) {
	return sqrt(innerProduct(x, x, length));
}

void DoubleArrays::add(double *x, double *y, int length, double *result) {
	for(int i=0; i<length; i++) {
		result[i] = x[i] + y[i];
	}
}

void DoubleArrays::subtract(double *x, double *y, int length, double *result) {
	for(int i=0; i<length; i++) {
		result[i] = x[i] - y[i];
	}
}

void DoubleArrays::exponentiate(double *pUnexponentiated, int length, double *result) {
	for(int i=0; i<length; i++) {
		result[i] = SloppyMath::exp(pUnexponentiated[i]);
	}
}

void DoubleArrays::truncate(double *x, double maxVal, int length) {
	for(int i=0; i<length; i++) {
		if(x[i] > maxVal)
			x[i] = maxVal;
		else if(x[i] < -maxVal)
			x[i] = -maxVal;
	}
}

void DoubleArrays::initialize(double *x, double d, int length) {
	for(int i=0; i<length; i++) {
		x[i] = d;
	}
}

