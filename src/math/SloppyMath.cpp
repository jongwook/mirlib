
#include <cfloat>
#include <cmath>
#include "SloppyMath.h"

double SloppyMath::logAdd(double logX, double logY) {
	// make logX the max
	if(logY > logX) {
		double temp = logX;
		logX = logY;
		logY = temp;
	}

	// now logX is bigger
	if(logX == -DBL_MAX) {
		return logX;
	}

	double negDiff = logY - logX;
	if(negDiff < -20) {
		return logX;
	}

	return logX + log(1.0 + ::exp(negDiff));
}

double SloppyMath::logAdd(double *logV, int length) {
	double max = -DBL_MAX;
	double maxIndex = 0;
	for (int i = 0; i < length; i++) {
		if (logV[i] > max) {
			max = logV[i];
			maxIndex = i;
		}
	}
	if (max == -DBL_MAX) return -DBL_MAX;
	// compute the negative difference
	double threshold = max - 20;
	double sumNegativeDifferences = 0.0;
	for (int i = 0; i < length; i++) {
		if(i != maxIndex && logV[i] > threshold) {
			sumNegativeDifferences += exp(logV[i] - max);
		}
	}
	if (sumNegativeDifferences > 0.0) {
		return max + log(1.0 + sumNegativeDifferences);
	}
	return max;
}

double SloppyMath::exp(double logX) {
	if(abs(logX) < 0.001)
		return 1 + logX;
	return ::exp(logX);
}

