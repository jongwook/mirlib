
#include <iostream>

#include "LBFGSMinimizer.h"
#include "BacktrackingLineSearcher.h"
#include "DoubleArrays.h"
#include "SloppyMath.h"

LBFGSMinimizer::LBFGSMinimizer(int _maxIterations /* = 20 */) {
	EPS = 1e-10;
	maxIterations = _maxIterations;
	maxHistorySize = 5;
	iterCallbackFunction = 0;
	minIterations = -1;
	initialStepSizeMultiplier = 0.01;
	stepSizeMultiplier = 0.5;
}

LBFGSMinimizer::~LBFGSMinimizer() {
	while(!inputDifferenceVectorList.empty()) {
		delete [] inputDifferenceVectorList.back();
		inputDifferenceVectorList.pop_back();
	}
	while(!derivativeDifferenceVectorList.empty()) {
		delete [] derivativeDifferenceVectorList.back();
		derivativeDifferenceVectorList.pop_back();
	}
}

int LBFGSMinimizer::minimize(DifferentiableFunction &function, double *initial, double tolerance, double *result, bool printProgress /* = true */) {
	BacktrackingLineSearcher lineSearcher;
	int dimension = function.dimension();
	double *guess = new double[dimension];
	DoubleArrays::assign(guess, initial, dimension);

	double value = function.valueAt(guess);
	double *derivative = new double[dimension];
	function.derivativeAt(guess, derivative);

	double *initialInverseHessianDiagonal = new double[dimension];
	double *direction = new double[dimension];


	for(int iteration=0; iteration < maxIterations; iteration++) {
		getInitialInverseHessianDiagonal(function, initialInverseHessianDiagonal, dimension);
		implicitMultiply(initialInverseHessianDiagonal, derivative, direction, dimension);
		DoubleArrays::scale(direction, -1.0, dimension);
		lineSearcher.stepSizeMultiplier = (iteration==0)?initialStepSizeMultiplier:stepSizeMultiplier;

		double *nextGuess = new double[dimension];
		double nextValue;
		lineSearcher.minimize(function, guess, direction, value, derivative, nextGuess, nextValue);
		double *nextDerivative = new double[dimension];
		function.derivativeAt(nextGuess, nextDerivative);

		if(printProgress) {
			std::cout << "[LBFGSMinimizer::minimize] Iteration " << iteration << " ended with value " << nextValue << std::endl;
		}

		if(iteration >= minIterations && converged(value, nextValue, tolerance)) {
			DoubleArrays::assign(result, nextGuess, dimension);
			delete [] guess;
			delete [] derivative;
			delete [] nextGuess;
			delete [] nextDerivative;
			delete [] initialInverseHessianDiagonal;
			delete [] direction;
			return 0;
		}

		updateHistories(guess, nextGuess, derivative, nextDerivative, dimension);

		delete [] guess;
		delete [] derivative;
		guess = nextGuess;
		value = nextValue;
		derivative = nextDerivative;

		if(iterCallbackFunction != 0) {
			iterCallbackFunction->iterationDone(guess, iteration);
		}
	}


	delete [] guess;
	delete [] derivative;
	delete [] initialInverseHessianDiagonal;
	delete [] direction;
	return 0;
}

bool LBFGSMinimizer::converged(double value, double nextValue, double tolerance) {
	if (value == nextValue) return true;
	double valueChange = SloppyMath::abs(nextValue - value);
	double valueAverage = SloppyMath::abs(nextValue + value + EPS) / 2.0;
	return (valueChange / valueAverage < tolerance);
}

void LBFGSMinimizer::updateHistories(double *guess, double *nextGuess, double *derivative, double *nextDerivative, int dimension) {
	double *guessChange = new double[dimension];
	double *derivativeChange = new double[dimension];
	DoubleArrays::subtract(nextGuess, guess, dimension, guessChange);
	DoubleArrays::subtract(nextDerivative, derivative, dimension, derivativeChange);
	pushOntoList(guessChange, inputDifferenceVectorList);
	pushOntoList(derivativeChange, derivativeDifferenceVectorList);
}

void LBFGSMinimizer::pushOntoList(double *vector, std::list<double *> &vectorList) {
	vectorList.push_front(vector);
	if((int)vectorList.size() > maxHistorySize) {
		delete [] vectorList.back();
		vectorList.pop_back();
	}
}

double* LBFGSMinimizer::getInputDifference(int num) {
	std::list<double *>::iterator it=inputDifferenceVectorList.begin();
	for(int i=0; i<num; i++, it++);
	return *it;
}

double* LBFGSMinimizer::getDerivativeDifference(int num) {
	std::list<double *>::iterator it=derivativeDifferenceVectorList.begin();
	for(int i=0; i<num; i++, it++);
	return *it;
}

int LBFGSMinimizer::implicitMultiply(double *initialInverseHessianDiagonal, double *derivative, double *result, int dimension) {
	int historySize = this->historySize();
	double *rho = new double[historySize];
	double *alpha = new double[historySize];
	double *right = new double[dimension];
	DoubleArrays::assign(right, derivative, dimension);
	for(int i = historySize-1; i >= 0; i--) {
		double *inputDifference = getInputDifference(i);
		double *derivativeDifference = getDerivativeDifference(i);
		rho[i] = DoubleArrays::innerProduct(inputDifference, derivativeDifference, dimension);
		if(rho[i] == 0.0) {
			std::cerr << "LBFGSMinimizer.implicitMultiply: Curvature problem.\n";
			delete [] rho;
			delete [] alpha;
			delete [] right;
			return -1;
		}
		alpha[i] = DoubleArrays::innerProduct(inputDifference, right, dimension) / rho[i];
		DoubleArrays::addMultiples(right, 1.0, derivativeDifference, -1.0*alpha[i], dimension, right);

	}
	DoubleArrays::pointwiseMultiply(initialInverseHessianDiagonal, right, dimension, result);
	for(int i = 0; i < historySize; i++) {
		double *inputDifference = getInputDifference(i);
		double *derivativeDifference = getDerivativeDifference(i);
		double beta = DoubleArrays::innerProduct(derivativeDifference, result, dimension) / rho[i];
		DoubleArrays::addMultiples(result, 1.0, inputDifference, alpha[i]-beta, dimension, result);
	}
	delete [] rho;
	delete [] alpha;
	delete [] right;

	return 0;
}

int LBFGSMinimizer::getInitialInverseHessianDiagonal(DifferentiableFunction &function, double *result, int dimension) {
	double scale = 1.0;
	if(derivativeDifferenceVectorList.size() >= 1) {
		double *lastDerivativeDifference = getLastDerivativeDifference();
		double *lastInputDifference = getLastInputDifference();
		double num = DoubleArrays::innerProduct(lastDerivativeDifference, lastInputDifference, dimension);
		double den = DoubleArrays::innerProduct(lastDerivativeDifference, lastDerivativeDifference, dimension);
		scale = num / den;
	}
	DoubleArrays::constantArray(scale, dimension, result);
	return 0;
}
