

#ifndef __math_DifferentiableFunction_h__
#define __math_DifferentiableFunction_h__

#include "Function.h"


class DifferentiableFunction: public Function {
public:
	virtual ~DifferentiableFunction() {}
	virtual void derivativeAt(double *x, double *result)=0;
};


#endif 
