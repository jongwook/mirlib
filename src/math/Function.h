
#ifndef __math_Function_h__
#define __math_Function_h__


class Function {
public:
	virtual ~Function() {}
	virtual int dimension()=0;
	virtual double valueAt(double *x)=0;
};


#endif 
