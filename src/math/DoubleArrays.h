

#ifndef __math_DoubleArrays_h__
#define __math_DoubleArrays_h__

#include <string>


class DoubleArrays {
public:
	static void assign(double *y, double *x, int length);
	static double innerProduct(double *x, double *y, int length);
	static void addMultiples(double *x, double xMultiplier, double *y, double yMultiplier, int length, double *result);
	static void constantArray(double c, int length, double *result);
	static void pointwiseMultiply(double *x, double *y, int length, double *result);
	static std::string toString(double *x, int length);
	static void scale(double *x, double s, int length);
	static void multiply(double *x, double s, int length, double *result);
	static int argMax(double *v, int length);
	static double max(double *v, int length);
	static int argMin(double *v, int length);
	static double min(double *v, int length);
	static double maxAbs(double *v, int length);
	static void add(double *a, double b, int length, double *result);
	static double add(double *a, int length);
	static double add(double *a, int first, int last);
	static double vectorLength(double *x, int length);
	static void add(double *x, double *y, int length, double *result);
	static void subtract(double *x, double *y, int length, double *result);
	static void exponentiate(double *pUnexponentiated, int length, double *result);
	static void truncate(double *x, double maxVal, int length);
	static void initialize(double *x, double d, int length);

};


#endif /* DOUBLEARRAYS_H_ */
