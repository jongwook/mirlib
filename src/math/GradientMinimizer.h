

#ifndef __math_GradientMinimizer_h__
#define __math_GradientMinimizer_h__

#include "DifferentiableFunction.h"


class GradientMinimizer {
public:
	virtual ~GradientMinimizer() {};
	virtual int minimize(DifferentiableFunction &function, double *initial, double tolerance, double *result)=0;
};



#endif 

