
#ifndef __math_SloppyMath_h__
#define __math_SloppyMath_h__


class SloppyMath {
public:
	static double min(int x, int y) { return (x>y)?y:x; }
	static double min(double x, double y) { return (x>y)?y:x; }
	static double max(int x, int y) { return (x>y)?x:y; }
	static double max(double x, double y) { return (x>y)?x:y; }
	static double abs(int x) { return (x>0)?x:-x; }
	static double abs(double x) { return (x>0)?x:-x; }
	static double logAdd(double logX, double logY);
	static double logAdd(double *logV, int length);
	static double exp(double logX);
};


#endif
