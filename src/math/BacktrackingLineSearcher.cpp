

#include <iostream>

#include "BacktrackingLineSearcher.h"
#include "DoubleArrays.h"


BacktrackingLineSearcher::BacktrackingLineSearcher() {
	EPS = 1e-10;
	stepSizeMultiplier = 0.9;
	sufficientDecreaseConstant = 1e-4;
}

BacktrackingLineSearcher::~BacktrackingLineSearcher() {
	// TODO Auto-generated destructor stub
}

int BacktrackingLineSearcher::minimize(DifferentiableFunction &function, double *initial, double *direction, double *result) {
	double initialValue = function.valueAt(initial);
	double *initialDerivative = new double[function.dimension()];
	function.derivativeAt(initial, initialDerivative);
	int ret = minimize(function, initial, direction, initialValue, initialDerivative, result, initialValue);
	delete [] initialDerivative;
	return ret;
}

int BacktrackingLineSearcher::minimize(DifferentiableFunction &function, double *initial, double *direction, double initialValue, double *initialDerivative, double *result, double &value) {
	double stepSize = 1.0;
	int dimension = function.dimension();
	double initialFunctionValue = initialValue;
	double *derivative = new double[dimension];
	DoubleArrays::assign(derivative, initialDerivative, dimension);
	double initialDirectionalDerivative = DoubleArrays::innerProduct(derivative, direction, dimension);
	double *guess = new double[dimension];
	double guessValue = 0.0;
	bool sufficientDecreaseObtained = false;

	while(!sufficientDecreaseObtained) {
		DoubleArrays::addMultiples(initial, 1.0, direction, stepSize, dimension, guess);
		guessValue = function.valueAt(guess);
		double sufficientDecreaseValue = initialFunctionValue + sufficientDecreaseConstant * initialDirectionalDerivative * stepSize;
		sufficientDecreaseObtained = (guessValue <= sufficientDecreaseValue);
		if(!sufficientDecreaseObtained) {
			stepSize *= stepSizeMultiplier;
			if(stepSize < EPS) {
				std::cerr << "BacktrackingSearcher.minimize: stepSize underflow.\n";
				DoubleArrays::assign(result, initial, dimension);
				value = initialValue;
				delete [] derivative;
				delete [] guess;
				return -1;
			}
		}
	}

	DoubleArrays::assign(result, guess, dimension);
	value = guessValue;
	delete [] derivative;
	delete [] guess;
	return 0;
}

