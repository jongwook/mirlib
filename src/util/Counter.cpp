
#include <cfloat>

#include "Counter.h"

template <class E>
Counter<E>::Counter() {
	currentModCount = 0;
	cacheModCount = -1;
	cacheTotalCount = 0.0;
}

template <class E>
Counter<E>::~Counter() {
	
}

template <class E>
typename Counter<E>::iterator Counter<E>::begin() {
	return entries.begin();
}

template <class E>
typename Counter<E>::const_iterator Counter<E>::begin() const {
	return entries.begin();
}

template <class E>
typename Counter<E>::iterator Counter<E>::end() {
	return entries.end();
}

template <class E>
typename Counter<E>::const_iterator Counter<E>::end() const {
	return entries.end();
}

template <class E>
int Counter<E>::size() const {
	return (int)entries.size();
}

template <class E>
bool Counter<E>::isEmpty() const {
	return entries.empty();
}

template <class E>
bool Counter<E>::containsKey(E key) const {
	return entries.count(key) != 0;
}

template <class E>
double Counter<E>::removeKey(E key) {
	if(containsKey(key)) {
		double d = entries[key];
		entries.erase(key);
		return d;
	}
	return 0.0;
}

template <class E>
double Counter<E>::getCount(E key) const {
	return (containsKey(key)) ? entries.at(key) : 0.0;
}

template <class E>
void Counter<E>::setCount(E key, double count) {
	currentModCount++;
	entries[key] = count;
}

template <class E>
void Counter<E>::incrementCount(E key, double increment) {
	setCount(key, getCount(key) + increment);
}

template <class E> 
void Counter<E>::incrementAll(const Counter<E> &counter) {
	for(const_iterator pair=counter.begin(); pair!=counter.end(); pair++) {
		incrementCount(pair->first, pair->second);
	}
}
	
template <class E> 
void Counter<E>::incrementAll(const std::vector<E> &counter) {
	for(typename std::vector<E>::const_iterator it=counter.begin(); it!=counter.end(); it++) {
		incrementCount(*it, 1.0);
	}
}
	
template <class E>
void Counter<E>::incrementAll(double increment) {
	for(iterator it=begin(); it!=end(); it++) {
		incrementCount(it->first, increment);
	}		
}

template <class E>
void Counter<E>::multiplyAll(double factor) {
	for(iterator it=begin(); it!=end(); it++) {
		setCount(it->first, it->second * factor);
	}		
}
	
template <class E> 
void Counter<E>::elementwiseMax(const Counter<E> &counter) {
	for(const_iterator pair=counter.begin(); pair!=counter.end(); pair++) {
		double count = counter.getCount(pair->first);
		if(getCount(pair->first) < count) {
			setCount(pair->first, pair->second);
		}
	}
}

template <class E>
double Counter<E>::totalCount() {
	if(currentModCount != cacheModCount) {
		double total = 0.0;
		for(iterator pair=entries.begin(); pair!=entries.end(); pair++) {
			total += pair->second;
		}
		cacheTotalCount = total;
		cacheModCount = currentModCount;
	}
	return cacheTotalCount;
}

template <class E>
void Counter<E>::normalize() {
	double total = totalCount();
	for(iterator pair=entries.begin(); pair!=entries.end(); pair++) {
		setCount(pair->first, getCount(pair->first) / total);
	}
}

template <class E>
E Counter<E>::argMax() const {
	double maxCount = -DBL_MAX;
	const E *maxKey=NULL;
	for(const_iterator pair=entries.begin(); pair!=entries.end(); pair++) {
		if(pair->second > maxCount) {
			maxKey = &(pair->first);
			maxCount = pair->second;
		}
	}
	return (maxKey != NULL)? *maxKey : E();
}

template <class E>
std::string Counter<E>::toString() const {
	return toString((int)entries.size());
}

template <class E>
std::string Counter<E>::toString(int maxKeysToPrint) const {
	return asPriorityQueue().toString(maxKeysToPrint);
}

template <class E>
PriorityQueue<E>& Counter<E>::asPriorityQueue() const {
	static FastPriorityQueue<E> pq;
	pq.clear();
	for(const_iterator pair=entries.begin(); pair!=entries.end(); pair++) {
		pq.setPriority(pair->first, pair->second);
	}
	return pq;
}
	
template class Counter<std::string>;
template class Counter<int>;

