
#ifndef __util_Indexer_h__
#define __util_Indexer_h__

#include <vector>
#include <unordered_map>

template <class E>
class Indexer {
public:
	Indexer() {} //=default;
	Indexer(const Indexer &) { std::cerr << "[Indexer] warning: using copy constructor\n";} // =delete;
	Indexer(Indexer&& other):
		objects(std::move(other.objects)), indices(std::move(other.indices)) {};
		virtual ~Indexer() {}//=default;
	
	Indexer& operator=(const Indexer &) { std::cerr << "[Indexer] warning: using copy assignment operator\n"; return *this; }//;=delete;
	Indexer& operator=(Indexer&& other) {
		std::swap(objects, other.objects);
		std::swap(indices, other.indices);
	}
	
	
	E get(int index) { return objects[index]; }
	int size() { return (int)objects.size(); }
	int indexOf(E e) { return (indices.count(e) != 0)?indices[e]:-1; }
	int addGetIndex(E e) {
		add(e);
		return indices[e];
	}
	bool contains(E e) { return (indices.count(e) != 0); }
	bool add(E e) {
		if(contains(e)) return false;
		objects.push_back(e);
		indices[e] = size()-1;
		return true;
	}
protected:
	std::vector<E> objects;
	std::unordered_map<E, int> indices;
};
	
#endif
