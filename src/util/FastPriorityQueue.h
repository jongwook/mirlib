
#ifndef __util_FastPriorityQueue_h__
#define __util_FastPriorityQueue_h__

#include "PriorityQueue.h"

template <class E>
class Counter;

/**
 * A priority queue based on a binary heap.
 * Note that this implementation does not efficiently support
 * containsKey or getPriority.  Removal is not supported.
 * If you set the priority of a key multiple times,
 * it will NOT be promoted or demoted, but rather it will be
 * inserted in the queue once multiple times, with the various
 * _priorities.
 */
template <class E>
class FastPriorityQueue: public PriorityQueue<E> {
public:
	FastPriorityQueue(int capacity = 15);
	virtual ~FastPriorityQueue();

	bool containsKey(E e);

	double getPriority(E e);

	double removeKey(E e);

	E removeFirst();

	/**
	 * Returns true if the priority queue is non-empty
	 */
	bool hasNext();

	/**
	 * Returns the element in the queue with highest priority, and pops it from the queue.
	 */
	E next();

	/**
	 * Returns the highest-priority element in the queue, but does not pop it.
	 */
	E getFirst();

	/**
	 * Gets the priority of the highest-priority element of the queue.
	 */
	double getPriority();

	/**
	 * Number of elements in the queue.
	 */
	int size() { return _size; }

	/**
	 * True if the queue is empty (size == 0).
	 */
	bool isEmpty() { return _size == 0; }

	/**
	 * Adds a key to the queue with the given priority.  If the key is already in the queue, it will be added an
	 * additional time, NOT promoted/demoted.
	 *
	 * @param key
	 * @param priority
	 */
	void setPriority(E key, double priority);

	/**
	 * Returns a representation of the queue in decreasing priority order.
	 */
	std::string toString() { return toString(size()); }

	/**
	 * Returns a representation of the queue in decreasing priority order, displaying at most maxKeysToPrint elements.
	 *
	 * @param maxKeysToPrint
	 */
	std::string toString(int maxKeysToPrint);

	/**
	 * Clears the queue
	 */
	void clear() { _size=0; }

protected:
	/**
	 * Returns a counter whose keys are the elements in this priority queue, and whose counts are the priorities in this
	 * queue.  In the event there are multiple instances of the same element in the queue, the counter's count will be the
	 * sum of the instances' priorities.
	 *
	 * @return
	 */
	Counter<E>& asCounter();

	/**
	 * Returns a clone of this priority queue.  Modifications to one will not affect modifications to the other.
	 */
	FastPriorityQueue<E>& deepCopy();

	void grow(int newCapacity);

	int parent(int loc) { return (loc - 1) / 2; }

	int leftChild(int loc) { return 2 * loc + 1; }

	int rightChild(int loc) { return 2 * loc + 2; }

	void heapifyUp(int loc);

	void heapifyDown(int loc);

	void swap(int loc1, int loc2);

	int _size;
	int _capacity;
	int _keep;
	E *_elements;
	double *_priorities;
};

#endif
