
#ifndef __util_Counter_h__
#define __util_Counter_h__

#include <map>
#include <set>
#include <string>
#include <vector>
#include <iostream>
#include <cmath>

#include "FastPriorityQueue.h"

/**
 * A map from objects to doubles.  Includes convenience methods for getting,
 * setting, and incrementing element counts.  Objects not in the counter will
 * return a count of zero.  The counter is backed by a HashMap (unless specified
 * otherwise with the MapFactory constructor).
 */
template<class E>
class Counter {
public:
	Counter();
	Counter(const Counter& other) { 
		if(other.size() > 0) {
			std::cerr << "[Counter] warning: using copy constructor\n";
		}
	} //=delete;
	Counter(Counter && other) throw():
		entries(/*std::move*/(other.entries)), 
		currentModCount(other.currentModCount),
		cacheModCount(other.cacheModCount),
		cacheTotalCount(other.cacheTotalCount) {}
	Counter(const std::vector<E> &other) { incrementAll(other); }
	Counter& operator=(const Counter&) { std::cerr << "[Counter] warning: using copy assignment operator\n"; return *this; } //=delete;
	Counter& operator=(Counter && other) throw() {
		std::swap(entries, other.entries);
		std::swap(currentModCount, other.currentModCount);
		std::swap(cacheModCount, other.cacheModCount);
		std::swap(cacheTotalCount, other.cacheTotalCount);
		return *this;
	}
	double& operator[](E key) { return entries[key]; }
	double operator[](E key) const { return getCount(key); }

	virtual ~Counter();

	typedef typename std::map<E,double>::iterator iterator;
	typedef typename std::map<E,double>::const_iterator const_iterator;

	/**
	 * iterators are used instead of keySet()
	 */
	iterator begin();
	const_iterator begin() const;
	iterator end();
	const_iterator end() const;

	/**
	 * The number of entries in the counter (not the total count -- use
	 * totalCount() instead).
	 */
	int size() const;

	/**
	 * True if there are no entries in the counter (false does not mean totalCount
	 * > 0)
	 */
	bool isEmpty() const;

	/**
	 * Returns whether the counter contains the given key.  Note that this is the
	 * way to distinguish keys which are in the counter with count zero, and those
	 * which are not in the counter (and will therefore return count zero from
	 * getCount().
	 *
	 * @param key
	 * @return whether the counter contains the key
	 */
	bool containsKey(E key) const;

	/**
	 * Remove a key from the counter. Returns the count associated with that
	 * key or zero if the key wasn't in the counter to begin with
	 * @param key
	 * @return the count associated with the key
	 */
	double removeKey(E key);

	/**
	 * Get the count of the element, or zero if the element is not in the
	 * counter.
	 *
	 * @param key
	 * @return
	 */
	double getCount(E key) const;

	/**
	 * Set the count for the given key, clobbering any previous count.
	 *
	 * @param key
	 * @param count
	 */
	void setCount(E key, double count);

	/**
	 * Increment a key's count by the given amount.
	 *
	 * @param key
	 * @param increment
	 */
	void incrementCount(E key, double increment);

	/**
	 * Increment each element in a given collection by a given amount.
	 */
	void incrementAll(const Counter<E> &counter);
	
	void incrementAll(const std::vector<E> &counter);
	
	/**
	 * Increment all elements by given count
	 */
	void incrementAll(double increment);
	
	/**
	 * Multiply all elements by give factor
	 */
	void multiplyAll(double factor);

	void elementwiseMax(const Counter<E> &counter);

	/**
	 * Finds the total of all counts in the counter.  This implementation uses
	 * cached count which may get out of sync if the entries map is modified in
	 * some unantipicated way.
	 *
	 * @return the counter's total
	 */
	double totalCount();

	/**
	 * Destructively normalize this Counter in place.
	 */
	void normalize();

	/**
	 * Finds the key with maximum count.  This is a linear operation, and ties are
	 * broken arbitrarily.
	 *
	 * @return a key with minumum count
	 */
	E argMax() const;

	/**
	 * Returns a string representation with the keys ordered by decreasing
	 * counts.
	 *
	 * @return string representation
	 */
	std::string toString() const;

	/**
	 * Returns a string representation which includes no more than the
	 * maxKeysToPrint elements with largest counts.
	 *
	 * @param maxKeysToPrint
	 * @return partial string representation
	 */
	std::string toString(int maxKeysToPrint) const;

	/**
	 * Returns a new counter having logarithmic count
	 */
	Counter log() const {
		Counter result;
		for(const_iterator it = begin(); it != end(); it++) {
			result.setCount(it->first, ::log(it->second));
		}
		return result;
	}
	
	/**
	 * Returns a new counter having exponential count
	 */
	Counter exp() const {
		Counter result;
		for(const_iterator it = begin(); it != end(); it++) {
			result.setCount(it->first, ::exp(it->second));
		}
		return /*std::move*/(result);
	}

	// getEntrySet has been omitted; iterators can be used as an alternative.

	/**
	 * Clears all entry in the counter
	 */
	void clear() { entries.clear(); }

	PriorityQueue<E>& asPriorityQueue() const;

protected:

	std::map<E,double> entries;
	int currentModCount;
	int cacheModCount;
	double cacheTotalCount;
};

#endif 
