
#include <cfloat>
#include <sstream>

#include "FastPriorityQueue.h"
#include "Counter.h"

/**
 * A priority queue based on a binary heap.
 * Note that this implementation does not efficiently support
 * containsKey or getPriority.  Removal is not supported.
 * If you set the priority of a key multiple times,
 * it will NOT be promoted or demoted, but rather it will be
 * inserted in the queue once multiple times, with the various
 * priorities.
 */

template <class E>
FastPriorityQueue<E>::FastPriorityQueue(int capacity /* = 15 */) {
	_size = 0;
	_elements = NULL;
	_priorities = NULL;

	int legalCapacity = 0;
	while(legalCapacity < capacity) {
		legalCapacity = 2 * legalCapacity + 1;
	}
	grow(legalCapacity);
}

template <class E>
FastPriorityQueue<E>::~FastPriorityQueue() {
	delete [] _elements;
	delete [] _priorities;
}

template <class E>
bool FastPriorityQueue<E>::containsKey(E e) {
	for(int i = 0; i < _size; i++) {
		if(_elements[i] == e) return true;
	}
	return false;
}

template <class E>
double FastPriorityQueue<E>::getPriority(E e) {
	double bestPriority = -DBL_MAX;
	for(int i = 0; i < _size; i++) {
		if(_elements[i] == e && _priorities[i] > bestPriority) {
			bestPriority = _priorities[i];
		}
	}
	return bestPriority;
}

template <class E>
double FastPriorityQueue<E>::removeKey(E e) {
	return 0.0;	// not supported
}

template <class E>
E FastPriorityQueue<E>::removeFirst() {
	if (_size < 1) return E();
	E element = _elements[0];
	swap(0, _size - 1);
	_size--;
	heapifyDown(0);
	return element;
}

template <class E>
bool FastPriorityQueue<E>::hasNext() {
	return !isEmpty();
}

template <class E>
E FastPriorityQueue<E>::next() {
	return removeFirst();
}

template <class E>
E FastPriorityQueue<E>::getFirst() {
	if (_size < 1) return _elements[0];
	return _elements[0];
}

template <class E>
double FastPriorityQueue<E>::getPriority() {
	if(size() > 0)
		return _priorities[0];
	return 0;
}

template <class E>
void FastPriorityQueue<E>::setPriority(E key, double priority) {
	if (_size == _capacity) {
		grow(2 * _capacity + 1);
	}
	_elements[_size] = key;
	_priorities[_size] = priority;
	heapifyUp(_size);
	_size++;
}

template <class E>
std::string FastPriorityQueue<E>::toString(int maxKeysToPrint) {
	PriorityQueue<E> &pq = deepCopy();
	std::stringstream ss;
	ss << "[";
	int numKeysPrinted = 0;
	while (numKeysPrinted < maxKeysToPrint && !pq.isEmpty()) {
		double priority = pq.getPriority();
		E element = pq.removeFirst();
		ss << element;
		ss << " : ";
		ss << priority;
		if(numKeysPrinted < size() - 1)
			ss << ", ";
		numKeysPrinted++;
	}
	if (numKeysPrinted < size())
		ss << "...";
	ss << "]";
	return ss.str();
}


template <class E>
Counter<E>& FastPriorityQueue<E>::asCounter() {
	PriorityQueue<E> &pq = deepCopy();
	static Counter<E> counter;
	counter.clear();
	while(!pq.isEmpty()) {
		double priority = pq.getPriority();
		E element = pq.removeFirst();
		counter.incrementCount(element, priority);
	}
	return counter;
}

template <class E>
FastPriorityQueue<E>& FastPriorityQueue<E>::deepCopy() {
	static FastPriorityQueue<E> clonePQ;
	clonePQ._size = _size;	// effectively empties the priority queue
	clonePQ._capacity = _capacity;
	if(clonePQ._elements)
		delete [] clonePQ._elements;
	if(clonePQ._priorities)
		delete [] clonePQ._priorities;
	clonePQ._elements = new E[_capacity];
	clonePQ._priorities = new double[_capacity];
	if(size()>0) {
		for(int i=0; i<_size; i++) {
			clonePQ._elements[i] = _elements[i];
			clonePQ._priorities[i] = _priorities[i];
		}
	}
	return clonePQ;
}


template <class E>
void FastPriorityQueue<E>::grow(int newCapacity) {
	E *newElements = new E[newCapacity];
	double *newPriorities = new double[newCapacity];

	if(_size > 0) {
		for(int i=0; i<_size; i++) {
			newElements[i] = _elements[i];
			newPriorities[i] = _priorities[i];
		}
	}

	if(!_elements)
		delete [] _elements;
	if(!_priorities)
		delete [] _priorities;

	_elements = newElements;
	_priorities = newPriorities;
	_capacity = newCapacity;
}

template <class E>
void FastPriorityQueue<E>::heapifyUp(int loc) {
	if (loc == 0) return;
	int _parent = parent(loc);
	if (_priorities[loc] > _priorities[_parent]) {
		swap(loc, _parent);
		heapifyUp(_parent);
	}
}

template <class E>
void FastPriorityQueue<E>::heapifyDown(int loc) {
	int max = loc;
	int _leftChild = leftChild(loc);
	if(_leftChild < size()) {
		double priority = _priorities[loc];
		double leftChildPriority = _priorities[_leftChild];
		if (leftChildPriority > priority)
			max = _leftChild;
		int _rightChild = rightChild(loc);
		if (_rightChild < size()) {
			double rightChildPriority = _priorities[rightChild(loc)];
			if (rightChildPriority > priority && rightChildPriority > leftChildPriority)
				max = _rightChild;
		}
	}
	if (max == loc)
		return;
	swap(loc, max);
	heapifyDown(max);
}

template <class E>
void FastPriorityQueue<E>::swap(int loc1, int loc2) {
	std::swap(_elements[loc1],_elements[loc2]);
	std::swap(_priorities[loc1],_priorities[loc2]);
}

template class FastPriorityQueue<std::string>;
template class FastPriorityQueue<int>;

