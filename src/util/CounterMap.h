
#ifndef __util_CounterMap_h__
#define __util_CounterMap_h__

#include "Counter.h"

template<class K, class V>
class CounterMap {
public:
	CounterMap();
	CounterMap(const CounterMap &other) {
		for(const_iterator it = other.begin(); it != other.end(); it++) {
			counterMap[it->first] = it->second;
		}
	}
	CounterMap(CounterMap && other) : counterMap(/*std::move*/(other.counterMap)) {}
	virtual ~CounterMap();

	CounterMap& operator=(const CounterMap &other) {
		counterMap.clear();
		counterMap = other.counterMap;
		return *this;
	}
	CounterMap& operator=(CounterMap && other) {
		std::swap(counterMap, other.counterMap);
		return *this;
	}

	Counter<V>& operator[](K key) { return getCounter(key); }

	typedef typename std::map<K,Counter<V> >::iterator iterator;
	typedef typename std::map<K,Counter<V> >::const_iterator const_iterator;

	/**
	 * iterators are used instead of keySet()
	 */
	iterator begin();
	const_iterator begin() const;
	iterator end();
	const_iterator end() const;

	/**
	 * Sets the count for a particular (key, value) pair.
	 */
	void setCount(K key, V value, double count);

	/**
	 * Increments the count for a particular (key, value) pair.
	 */
	void incrementCount(K key, V value, double count);

	/**
	 * Gets the count of the given (key, value) entry, or zero if that entry is
	 * not present.  Does not create any objects.
	 */
	double getCount(K key, V value);

	/**
	 * Gets the sub-counter for the given key.  If there is none, a counter is
	 * created for that key, and installed in the CounterMap.  You can, for
	 * example, add to the returned empty counter directly (though you shouldn't).
	 * This is so whether the key is present or not, modifying the returned
	 * counter has the same effect (but don't do it).
	 */
	Counter<V>& getCounter(K key);

	/**
	 * Returns whether or not the <code>CounterMap</code> contains any
	 * entries for the given key.
	 * @author Aria Haghighi
	 * @param key
	 * @return
	 */
	bool containsKey(K key);

	/**
	 * Returns the total of all counts in sub-counters.  This implementation is
	 * caches the result -- it can get out of sync if the entries get modified externally.
	 */
	double totalCount();

	/**
	 * Returns the total number of (key, value) entries in the CounterMap (not
	 * their total counts).
	 */
	int totalSize();

	/**
	 * Normalizes the maps inside this CounterMap -- not the CounterMap itself.
	 */
	void normalize();

	/**
	 * The number of keys in this CounterMap (not the number of key-value entries
	 * -- use totalSize() for that)
	 */
	int size();

	/**
	 * True if there are no entries in the CounterMap (false does not mean
	 * totalCount > 0)
	 */
	bool isEmpty();

	/**
	 * Returns a new counter map containing this's logarithm
	 */
	CounterMap log() const {
		CounterMap result;
		for(const_iterator it = begin(); it != end(); it++) {
			result.counterMap[it->first] = it->second.log();
		}
		return /*std::move*/(result);
	}

	std::string toString() const;


protected:
	Counter<V>& ensureCounter(K key);

	std::map<K,Counter<V> > counterMap;
	int currentModCount;
	int cacheModCount;
	double cacheTotalCount;
};

#endif 

