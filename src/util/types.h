
#ifndef __util_types_h__
#define __util_types_h__

template<typename T> struct is_floating { enum { value = false }; };
template<> struct is_floating<float> { enum { value = true }; };
template<> struct is_floating<double> { enum { value = true }; };

#endif