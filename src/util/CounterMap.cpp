
#include <sstream>

#include "CounterMap.h"

template <class K, class V>
CounterMap<K,V>::CounterMap() {
	currentModCount = 0;
	cacheModCount = -1;
	cacheTotalCount = 0.0;
}

template <class K, class V>
CounterMap<K,V>::~CounterMap() {
}

template <class K, class V>
typename CounterMap<K,V>::iterator CounterMap<K,V>::begin() {
	return counterMap.begin();
}

template <class K, class V>
typename CounterMap<K,V>::const_iterator CounterMap<K,V>::begin() const {
	return counterMap.begin();
}

template <class K, class V>
typename CounterMap<K,V>::iterator CounterMap<K,V>::end() {
	return counterMap.end();
}

template <class K, class V>
typename CounterMap<K,V>::const_iterator CounterMap<K,V>::end() const {
	return counterMap.end();
}

template <class K, class V>
void CounterMap<K,V>::setCount(K key, V value, double count) {
	Counter<V> &valueCounter = ensureCounter(key);
	valueCounter.setCount(value, count);
	currentModCount++;
}

template <class K, class V>
void CounterMap<K,V>::incrementCount(K key, V value, double count) {
	Counter<V> &valueCounter = ensureCounter(key);
	valueCounter.incrementCount(value, count);
	currentModCount++;
}

template <class K, class V>
double CounterMap<K,V>::getCount(K key, V value) {
	if(counterMap.count(key) == 0)
		return 0.0;
	return counterMap[key].getCount(value);
}

template <class K, class V>
Counter<V>& CounterMap<K,V>::getCounter(K key) {
	return ensureCounter(key);
}

template <class K, class V>
bool CounterMap<K,V>::containsKey(K key) {
	return counterMap.count(key) != 0;
}

template <class K, class V>
double CounterMap<K,V>::totalCount() {
	if (currentModCount != cacheModCount) {
		double total = 0.0;
		for(iterator pair = begin(); pair != end(); pair++) {
			total += pair->second.totalCount();
		}
		cacheTotalCount = total;
		cacheModCount = currentModCount;
	}
	return cacheTotalCount;
}

template <class K, class V>
int CounterMap<K,V>::totalSize() {
	int total = 0;
	for(iterator pair = begin(); pair != end(); pair++) {
		total += pair->second.size();
	}
	return total;
}

template <class K, class V>
void CounterMap<K,V>::normalize() {
	for(typename CounterMap<K,V>::iterator pair = begin(); pair != end(); pair++) {
		pair->second.normalize();
	}
	currentModCount++;
}

template <class K, class V>
int CounterMap<K,V>::size() {
	return (int)counterMap.size();
}

template <class K, class V>
bool CounterMap<K,V>::isEmpty() {
	return size() == 0;
}

template <class K, class V>
std::string CounterMap<K,V>::toString() const {
	std::stringstream ss;
	ss << "[\n";
	for(const_iterator pair = begin(); pair != end(); pair++) {
		ss << " " << pair->first << " -> " << pair->second.toString() << "\n";
	}
	ss << "]";
	return ss.str();
}

template <class K, class V>
Counter<V>& CounterMap<K,V>::ensureCounter(K key) {
	Counter<V> &valueCounter = counterMap[key];
	return valueCounter;
}

template class CounterMap<std::string, std::string>;
template class CounterMap<int,int>;
