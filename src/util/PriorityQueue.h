
#ifndef __util_PriorityQueue_h__
#define __util_PriorityQueue_h__

#include <string>

template<class E>
class PriorityQueue {
public:
	virtual ~PriorityQueue() {}
	virtual bool hasNext()=0;
	virtual E next()=0;
	virtual E getFirst()=0;
	virtual E removeFirst()=0;
	virtual double getPriority()=0;
	virtual bool containsKey(E element)=0;
	virtual double removeKey(E element)=0;
	virtual void setPriority(E element, double priority)=0;
	virtual double getPriority(E element)=0;
	virtual std::string toString()=0;
	virtual std::string toString(int maxKeysToPrint)=0;
	virtual int size()=0;
	virtual bool isEmpty()=0;
};

#endif
