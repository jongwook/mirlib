
#ifndef __mir_YIN_h__
#define __mir_YIN_h__

#include <map>
#include <iostream>

#include "Eigen/Eigen"
using namespace Eigen;

class YIN {
public:
	struct DetectedPitchHandler {
		virtual void handleDetectedPitch(double time, double pitch)=0;
	};

	struct PrintDetectedPitchHandler: public DetectedPitchHandler {
		void handleDetectedPitch(double time, double pitch) {
			std::cout << time << "\t" << pitch << std::endl;
		}
	};
	
	YIN() : bufferSize(1024), overlapSize(bufferSize/2), inputBuffer(bufferSize), yinBuffer(bufferSize/2) {}
	
	static void processAudio(const ArrayXd &audio, int sampleRate, DetectedPitchHandler &detectedPitchHandler) {processAudio(audio, sampleRate, static_cast<DetectedPitchHandler&&>(detectedPitchHandler)); }
	static void processAudio(const ArrayXd &audio, int sampleRate, DetectedPitchHandler &&detectedPitchHandler = PrintDetectedPitchHandler());

	static std::map<double, int> transcribeAudio(const ArrayXd &audio, int sampleRate);

private:
	void difference();
	void cumulativeMeanNormalizedDifference();
	int absoluteThreshold();
	double parabolicInterpolation(int tauEstimate);
	double getPitch(int sampleRate);

	static const double threshold;
	const int bufferSize;
	const int overlapSize;

	ArrayXd inputBuffer;
	ArrayXd yinBuffer;


};

#endif
