//
//  Chord.h
//  mlutils
//
//

#ifndef mlutils_Chord_h
#define mlutils_Chord_h

#include <string>
#include <vector>
#include <map>
#include <bitset>

class Chord {
	
public:
	Chord();
	Chord(const std::string &name);
	Chord(const std::string &relative_name, int tonic);
	Chord(const Chord &other) : _root(other._root), _bass(other._bass), _components(other._components) {}
	Chord(Chord &&other) : _root(other._root), _bass(other._bass), _components(/*std::move*/(other._components)) {}
	
	Chord& operator=(const Chord &other) { _root = other._root; _bass = other._bass; _components=other._components; return *this; }
	Chord& operator=(Chord &&other) { _root = other._root; _bass = other._bass; _components=/*std::move*/(other._components); return *this; }

	int root() const { return _root; }
	int bass() const { return _bass; }
	int quality() const;	// major : 0, minor : 1, none : -1
	enum { none=-1, major=0, minor=1 };

	std::string name(int tonic = -1) const;
	std::bitset<12> components();
	std::vector<double> template_vector();
	
	static std::map<std::string, int> pitchclass_to_number;

	static const char *notes[];					// pitch classes : A, A#, ...
	static const char *degrees[];				// # halfsteps -> degree name
	static const char *shorthands[];			// shorthand names
	static const int shorthand_components[];	// shorthand index -> #halfsteps of each component, packed
	static const int degree_to_interval[];		// degree (1, 2, 3, 4, 5, 6, 7) to interval (0, 2, 4, 5, 7, 9, 11)

	static void init();
	void init(const std::string& name);
private:
	int _root;	// N=-1, A=0, A#=1, B=2, ..., G=10, G#=11
	std::vector<int> _components;
	int _bass;	// bass note in # of semitones from the root, nonzero for inversion
	
	
	static bool initialized;
	
	static int nShorthands;
	static int nDegrees;
	
	static std::map<int, std::string> components_to_shorthand;
	static std::map<std::string, int> shorthand_to_components;
};

#endif
