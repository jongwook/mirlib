
#include "YIN.h"

static const double LOG2 = 0.693147181;

const double YIN::threshold = 0.15;


void YIN::processAudio(const ArrayXd &audio, int sampleRate, DetectedPitchHandler &&detectedPitchHandler /* = PrintDetectedPitchHandler() */) {
	double time = 0;
		
	YIN yinInstance;
	int bufferSize = yinInstance.inputBuffer.size();
	int bufferStepSize = yinInstance.bufferSize - yinInstance.overlapSize;

	//read full buffer
		
	for (int start = 0; start + bufferSize < audio.size(); start += bufferStepSize) {
		yinInstance.inputBuffer = audio.block(start, 0, bufferSize, 1);
		double pitch = yinInstance.getPitch(sampleRate);

		time = (double)(start + bufferSize/2) / sampleRate;
		detectedPitchHandler.handleDetectedPitch(time,pitch);
	}
}

std::map<double, int> YIN::transcribeAudio(const ArrayXd &audio, int sampleRate) {
	struct TranscribeHandler: public DetectedPitchHandler {
		void handleDetectedPitch(double time, double pitch) {
			data[time]=pitch;
		};
		std::map<double, double> data;
	};

	// detect pitch
	TranscribeHandler handler;
	processAudio(audio, sampleRate, handler);
	std::map<double, double> &data = handler.data;

	// find the mean tuning 
	double avg_tune = 0;
	int count = 0;
	for (auto pair = data.begin(); pair != data.end(); pair++) {
		// skip if not detected 
		if(pair->second < 0) continue;

		double tune = ::log(pair->second/440.0)/LOG2 * 12;
		tune -= ::floor(tune);

		avg_tune *= (double)count/(count+1);
		avg_tune += tune/(count+1);

		if(avg_tune > 1.0) avg_tune -= 1.0;

		count++;
	}
	
	if(avg_tune > 0.5) avg_tune -= 1.0;
	//std::cout << "average tune : " << avg_tune << std::endl;
	
	// the tuning will be adjusted by multiplying this value
	double tune_multiplier = 1.0; // ****** HACK: not doing the tuning adjustment ******  ::pow(2.0, -avg_tune / 12.0);

	std::map<double, int> result;
	int last_pitch = -1;
	int stop_threshold = 0;

	for (auto pair = data.begin(); pair != data.end(); pair++) {
		int midi_note = -1;
		if (pair->second > 0) {
			double pitch = 12.0 * ::log(pair->second * tune_multiplier / 440.0) / LOG2 + 69.0;
			midi_note = (int)(pitch+0.5);
			if (midi_note < 0) midi_note = 0;
		}
		
		//if( (tmp = (tmp+1)%3) == 0) std::cout << pair->first << "\t" << pair->second << "\t" << midi_note << std::endl;

		// if this pitch is different from the previous one
		if (midi_note % 12 != last_pitch) {
			stop_threshold++;

			// and if it happens 5 consecutive times
			if(stop_threshold >= 5) {
				last_pitch = midi_note % 12;
				result[pair->first] = midi_note;

				stop_threshold = 0;
			}
		}
	}
	
	return /*std::move*/(result);
}

void YIN::difference() {
	for (int tau = 0; tau < yinBuffer.size(); tau++) {
		yinBuffer[tau] = 0;
	}
	for (int tau = 1; tau < yinBuffer.size(); tau++) {
		for (int j = 0; j < yinBuffer.size(); j++) {
			double delta = inputBuffer[j] - inputBuffer[j+tau];
			yinBuffer[tau] += delta * delta;
		}
	}
}

void YIN::cumulativeMeanNormalizedDifference() {
	yinBuffer[0] = 1;
	double runningSum = yinBuffer[1];
	yinBuffer[1] = 1;
	for (int tau = 2; tau < yinBuffer.size(); tau++) {
		runningSum += yinBuffer[tau];
		yinBuffer[tau] *= tau / runningSum;
	}
}

int YIN::absoluteThreshold() {
	for (int tau = 1; tau < yinBuffer.size(); tau++) {
		if (yinBuffer[tau] < threshold) {
			while (tau+1 < yinBuffer.size() && yinBuffer[tau+1] < yinBuffer[tau]) {
				tau ++;
			}
			return (int)tau;
		}
	}
	return -1;
}

double YIN::parabolicInterpolation(int tauEstimate) {
	double s0, s1, s2;
	int x0 = (tauEstimate < 1) ? tauEstimate : tauEstimate - 1;

	int x2 = (tauEstimate + 1 < (int)yinBuffer.size()) ? tauEstimate + 1 : tauEstimate;
	if (x0 == tauEstimate)
		return (yinBuffer[tauEstimate] <= yinBuffer[x2]) ? tauEstimate : x2;
	if (x2 == tauEstimate)
		return (yinBuffer[tauEstimate] <= yinBuffer[x0]) ? tauEstimate : x0;
	s0 = yinBuffer[x0];
	s1 = yinBuffer[tauEstimate];
	s2 = yinBuffer[x2];

	return tauEstimate + 0.5f * (s2 - s0 ) / (2.0f * s1 - s2 - s0);
}

double YIN::getPitch(int sampleRate) {
	int tauEstimate = -1;
	double pitchInHertz = -1;

	//step 2
	difference();

	//step 3
	cumulativeMeanNormalizedDifference();

	//step 4
	tauEstimate = absoluteThreshold();

	//step 5
	if(tauEstimate != -1){
		double betterTau = parabolicInterpolation(tauEstimate);

		//step 6
		//TODO Implement optimization for the YIN algorithm.
		//0.77% => 0.5% error rate,
		//using the data of the YIN paper
		//bestLocalEstimate()

		//conversion to Hz
		pitchInHertz = (double)sampleRate/betterTau;
	}

	return pitchInHertz;
}


