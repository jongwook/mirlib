
#include <algorithm>
#include <sstream>
#include <iostream>

#include "Chord.h"

#define PACK2(a,b)			( (a) | ( (b) << 5 ) )
#define PACK3(a,b,c)		( (a) | ( (b) << 5 ) | ( (c) << 10 ) )
#define PACK4(a,b,c,d)		( (a) | ( (b) << 5 ) | ( (c) << 10 ) | ( (d) << 15 ) )
#define PACK5(a,b,c,d,e)	( (a) | ( (b) << 5 ) | ( (c) << 10 ) | ( (d) << 15 ) | ( (e) << 20 ) )
#define PACK6(a,b,c,d,e,f)	( (a) | ( (b) << 5 ) | ( (c) << 10 ) | ( (d) << 15 ) | ( (e) << 20 ) | ( (f) << 25 ) )

#define UNPACK(pack, i)		( ( (pack) & (0x1f << (i)*5) ) >> (i)*5 )



// pitch classes : A, A#, ...
const char* Chord::notes[] = {"A", "Bb", "B", "C", "C#", "D", "Eb", "E", "F", "F#", "G", "Ab"};
	
// # halfsteps -> degree name
const char* Chord::degrees[] = {"1","b2","2","b3","3","4","b5","5","#5","6","b7","7","8","b9","9","b10","10","11","b12","12","b13","13","#13"};
	
// degree (1, 2, 3, 4, 5, 6, 7, ...) to interval (0, 2, 4, 5, 7, 9, 11, ///)
const int Chord::degree_to_interval[] = {0, 0, 2, 4, 5, 7, 9, 11, 12, 14, 16, 17, 19, 21};

// shorthand names
const char* Chord::shorthands[] = {
	"maj","min","dim","aug","maj7","min7","7","dim7","hdim7","minmaj7","maj6","min6","9","maj9","min9","sus4",	// from the original paper
	"1", "5", "sus2", "maj11", "11", "min11", "maj13", "13", "min13"											// McGill Billboard Project additions
};	
	
// shorthand index -> #halfsteps of each component
const int Chord::shorthand_components[] = { 
	PACK2(4, 7), // maj
	PACK2(3, 7), // min
	PACK2(3, 6), // dim
	PACK2(4, 8), // aug
		
	PACK3(4, 7, 11), // maj7
	PACK3(3, 7, 10), // min7
	PACK3(4, 7, 10), // 7
	PACK3(3, 6,  9), // dim7
	PACK3(3, 6, 10), // hdim7
	PACK3(3, 7, 11), // minmaj7
		
	PACK3(4, 7, 9), // maj6
	PACK3(3, 7, 9), // min6
		
	PACK4(4, 7, 10, 14), // 9
	PACK4(4, 7, 11, 14), // maj9
	PACK4(3, 7, 10, 14), // min9
		
	PACK2(5, 7),	// sus4

	0,				// monophonic
	7,				// power chord
	PACK2(2, 7),	// sus2
		
	PACK5(4, 7, 11, 14, 17),	// maj11
	PACK5(4, 7, 10, 14, 17),	// 11
	PACK5(3, 7, 10, 14, 17),	// min11
		
	PACK6(4, 7, 11, 14, 17, 21),	// maj13
	PACK6(4, 7, 10, 14, 17, 21),	// 13
	PACK6(3, 7, 10, 14, 17, 21),	// min13
};

bool Chord::initialized = false;
	
int Chord::nShorthands;
int Chord::nDegrees;
	
std::map<int, std::string> Chord::components_to_shorthand;
std::map<std::string, int> Chord::shorthand_to_components;
std::map<std::string, int> Chord::pitchclass_to_number;
	
void Chord::init() {
	if(initialized) return;
	
	nShorthands	= sizeof(shorthands) / sizeof(shorthands[0]);
	nDegrees	= sizeof(degrees)	 / sizeof(degrees[0]);
		
	for(int i=0; i<nShorthands; i++) {
		shorthand_to_components[shorthands[i]] = shorthand_components[i];
		components_to_shorthand[shorthand_components[i]] = shorthands[i];
	}
		
	pitchclass_to_number["A"]=0;
	pitchclass_to_number["A#"]=1;
	pitchclass_to_number["Bb"]=1;		
	pitchclass_to_number["B"]=2;		
	pitchclass_to_number["C"]=3;		
	pitchclass_to_number["C#"]=4;
	pitchclass_to_number["Db"]=4;				
	pitchclass_to_number["D"]=5;		
	pitchclass_to_number["D#"]=6;
	pitchclass_to_number["Eb"]=6;		
	pitchclass_to_number["E"]=7;		
	pitchclass_to_number["F"]=8;		
	pitchclass_to_number["F#"]=9;
	pitchclass_to_number["Gb"]=9;		
	pitchclass_to_number["G"]=10;		
	pitchclass_to_number["G#"]=11;
	pitchclass_to_number["Ab"]=11;
		
	initialized = true;
}

void Chord::init(const std::string &name) {
	_root = -1;
	_bass = 0;
		
	std::vector<char> namecopy(name.length()+1);
	std::copy(name.begin(), name.end(), namecopy.begin());
	namecopy[name.length()] = '\0';
		
	size_t pos;
		
	// read bass note
	pos = name.find('/');
	if(pos != name.npos) {
		namecopy[pos] = '\0';
		char *p = &namecopy[pos] + 1;
		int mod = 0;
		while(*p == '#' || *p == 'b') {
			if(*p == '#') mod++;
			if(*p == 'b') mod--;
			p++;
		}
		int degree = 0;
		while(*p >= '0' && *p <= '9') {
			degree *= 10;
			degree += *p - '0';
			p++;
		}
			
		if(degree < 0 || degree > 13) 
			return; // invalid degree
			
		_bass = (degree_to_interval[degree]+mod+12) % 12;
	}

	// read chord
	pos = name.find(':');
	if(pos == name.npos) {
		// major chord
		_components.push_back(4);
		_components.push_back(7); 
	} else {
		namecopy[pos] = '\0';
		char *p = &namecopy[pos]+1;
			
		if(*p != '(') {
			// shorthand
			pos = name.find('(');
			if(pos != name.npos) namecopy[pos] = '\0';
				
			if(shorthand_to_components.count(p)==0)
				return;	// unknown shorthand
				
			int packed = shorthand_to_components[p];
			int unpacked[6] = { UNPACK(packed, 0), UNPACK(packed, 1), UNPACK(packed, 2), UNPACK(packed, 3), UNPACK(packed, 4), UNPACK(packed, 5) };
			for(int i=0; i<6; i++) {
				if(unpacked[i] != 0) {
					_components.push_back(unpacked[i]);
				}
			}
		} else {
			pos = name.find('(');
		}

		if(pos != name.npos) {
			// degree list
			p = &namecopy[pos]+1;
			while(*p != ')' && *p != '\0') {
				int mod = 0;
				bool omit = false;
				if(*p == '*') {
					omit = true;
					p++;
				}
				while(*p == '#' || *p == 'b') {
					if(*p == '#') mod++;
					if(*p == 'b') mod--;
					p++;
				}
				int degree = 0;
				while(*p >= '0' && *p <= '9') {
					degree *= 10;
					degree += *p - '0';
					p++;
				}
					
				int interval = degree_to_interval[degree]+mod;
				auto it = std::find(_components.begin(), _components.end(), interval);
					
				if(it != _components.end() && omit) 
					_components.erase(it);
					
				if(it == _components.end() && !omit)
					_components.push_back(interval);
					
				p++;
			}
		}
	}
		
	if(pitchclass_to_number.count(&namecopy[0]) == 0)
		return;
		
	_root = pitchclass_to_number[&namecopy[0]];
}
	
Chord::Chord() {
	init();
	_root = -1;
}
	
Chord::Chord(const std::string &name) {
	init();
	init(name);
}

Chord::Chord(const std::string &relative_name, int tonic) {
	init();
	size_t pos = relative_name.find(':');
	if(pos == relative_name.npos) pos = relative_name.length();
	std::string root = relative_name.substr(0, pos);
	std::string remainder = relative_name.substr(pos);

	int semitones = 0;
	for(size_t i=0; i<root.length(); i++) {
		if(root[i] == 'b') {
			semitones--;
		} else if(root[i] == '#') {
			semitones++;
		}
	}
	int degree = root[root.length()-1] - '0';
	root = notes[ (tonic + degree_to_interval[degree] + semitones) % 12];

	init(root + remainder);
}

int Chord::quality() const {
	if(_root == -1) return -1;
		
	if(std::find(_components.begin(), _components.end(), 4) != _components.end()) 
		return 0;	// major 
	else if(std::find(_components.begin(), _components.end(), 3) != _components.end())
		return 1;	// minor
		
	return 3;	// something else
}
	
std::bitset<12> Chord::components() {
	std::bitset<12> result;
	if(_root == -1) return /*std::move*/(result);
	result[_root] = true;
	for(auto it = _components.begin(); it != _components.end(); it++) {
		result[(_root + *it)%12] = true;
	}
	result[(_root + _bass)%12] = true;
	return /*std::move*/(result);
}
	
std::vector<double> Chord::template_vector() {
	std::vector<double> result(12);
	if(_root == -1) return /*std::move*/(result);
	result[_root] = 2.0;
	for(auto it = _components.begin(); it != _components.end(); it++) {
		result[(_root + *it)%12] = (_components.size() <= 2) ? 1.0 : 0.5;
	}
	if(_bass != 0) {
		result[(_root + _bass)%12] = 1.0;// / _components.size();
	}
	return /*std::move*/(result);
}

std::string Chord::name(int tonic /* = -1 */) const {
	std::stringstream ss;
	if (_root == -1) return "N";
	if (tonic == -1) {
		ss << notes[_root];
	} else {
		ss << degrees[(_root + 12 - tonic)%12];
	}
		
	size_t numComponents = _components.size();
	if ( !(numComponents == 2 && _components[0]==4 && _components[1]==7) ) { // skip components if major chord
		ss << ":";
		int pack0 = (numComponents==0) ? 0 : -1;
		int pack1 = (numComponents>=1) ? _components[0] : -1;
		int pack2 = (numComponents>=2) ? PACK2(_components[0], _components[1]) : -1;
		int pack3 = (numComponents>=3) ? PACK3(_components[0], _components[1], _components[2]) : -1;
		int pack4 = (numComponents>=4) ? PACK4(_components[0], _components[1], _components[2], _components[3]) : -1;
		int pack5 = (numComponents>=5) ? PACK5(_components[0], _components[1], _components[2], _components[3], _components[4]) : -1;
		int pack6 = (numComponents>=6) ? PACK6(_components[0], _components[1], _components[2], _components[3], _components[4], _components[5]) : -1;
			
		size_t more = 0;
		if (pack6 != -1 && components_to_shorthand.count(pack6) != 0) {
			ss << components_to_shorthand[pack6];
			more = 6;
		} else if (pack5 != -1 && components_to_shorthand.count(pack5) != 0) {
			ss << components_to_shorthand[pack5];
			more = 5;
		} else if (pack4 != -1 && components_to_shorthand.count(pack4) != 0) {
			ss << components_to_shorthand[pack4];
			more = 4;
		} else if (pack3 != -1 && components_to_shorthand.count(pack3) != 0) {
			ss << components_to_shorthand[pack3];
			more = 3;
		} else if (pack2 != -1 && components_to_shorthand.count(pack2) != 0) {
			ss << components_to_shorthand[pack2];
			more = 2;
		} else if (pack1 != -1 && components_to_shorthand.count(pack1) != 0) {
			ss << components_to_shorthand[pack1];
			more = 1;
		} else if (pack0 != -1) {
			ss << components_to_shorthand[pack0];
		}
			
		if (more < numComponents) {
			ss << "(";
			for(size_t i=more; i<numComponents; i++) {
				int c = _components[i];
				ss << degrees[c];
				ss << ( (i+1 == numComponents)?")":"," );
			}
		}
	}
		
	if (_bass != 0) {
		ss << "/" << degrees[_bass];
	}
			
	return ss.str();
}
