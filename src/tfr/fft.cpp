
#include <algorithm>
#include <iostream>

#include "fft.h"

ArrayXcd fft(const ArrayXcd &source) {
	ArrayXcd result(source.size());
	fft(&source[0], &result[0], source.size());
	return result;
}

void fft(const std::vector<std::complex<double> > &source, std::vector<std::complex<double> > &dest) {
	int length = (int)source.size();
	dest.resize(length);
	fft(&source[0], &dest[0], length);
}

void fft(const std::complex<double> *source, std::complex<double> *dest, int length) {
	kissfft<double> fft(length);
	fft.transform(source, dest);
}

ArrayXd fft(const ArrayXd& source) {
	ArrayXd result(source.size());
	fft(&source[0], &result[0], source.size());
	return result;
}

void fft(const std::vector<double> &source, std::vector<double> &dest) {
	int length = (int)source.size();
	dest.resize(length);
	fft(&source[0], &dest[0], length);
}

void fft(const double *source, double *dest, int length) {
	kissfft<double> fft(length);
	std::vector<std::complex<double> > src(length), dst(length);
	for(int i=0; i<length; i++) {
		src[i] = std::complex<double>(source[i]);
	}
	fft.transform(&src[0], &dst[0]);
	for(int i=0; i<length; i++) {
		dest[i] = abs(dst[i]);
	}
}

ArrayXcd ifft(const ArrayXcd &source) {
	ArrayXcd result(source.size());
	ifft(&source[0], &result[0], source.size());
	return result;
}
	
void ifft(const std::vector<std::complex<double> > &source, std::vector<std::complex<double> > &dest) {
	int length = (int)source.size();
	dest.resize(length);
	ifft(&source[0], &dest[0], length);
}

void ifft(const std::complex<double> *source, std::complex<double> *dest, int length) {
	kissfft<double> fft(length, true);
	fft.transform(source, dest);
}

ArrayXcd rfft(const ArrayXd &source) {
	ArrayXcd result(source.size()/2+1);
	rfft(&source[0], &result[0], source.size());
	return result;
}
	
void rfft(const std::vector<double> &source, std::vector<std::complex<double> > &dest) {
	int length = (int)source.size();
	dest.resize(length/2+1);
	rfft(&source[0], &dest[0], length);
}

void rfft(const double *source, std::complex<double> *dest, int length) {
	kissfft<double> fft(length);
	std::vector<std::complex<double> > src(length), dst(length);
	for(int i=0; i<length; i++) {
		src[i] = std::complex<double>(source[i], 0);
	}
	fft.transform(&src[0], &dst[0]);
	dst.resize(length/2+1);
	std::copy(dst.begin(), dst.end(), dest);
}

ArrayXd irfft(const ArrayXcd &source) {
	ArrayXd result(2*(source.size()-1));
	irfft(&source[0], &result[0], source.size());
	return result;
}

void irfft(const std::vector<std::complex<double> > &source, std::vector<double> &dest) {
	int length = (int)source.size();
	dest.resize(2*(length-1));
	irfft(&source[0], &dest[0], length);
}

void irfft(const std::complex<double> *source, double *dest, int length) {
	std::vector<std::complex<double> > src(2*(length-1)), dst(2*(length-1));
	std::copy(source, source+length, src.begin());
	std::reverse_copy(source+1, source+length-1, &src[length]);
	for(int i=length; i<2*(length-1); i++) {
		src[i] = std::conj(src[i]);
	}
	ifft(src,dst);
	for(int i=0; i<2*(length-1); i++) {
		dest[i] = dst[i].real();
	}
}

template<int type>
ArrayXd dct(const ArrayXd &source) {
	ArrayXd result(source.size());
	dct<type>(&source[0], &result[0], source.size());
	return result;
}

template<int type>
void dct(const std::vector<double> &source, std::vector<double> &dest) {
	//static_assert(type >= 1 && type <= 3, "unsupported dct type");
	int length = (int)source.size();
	dest.resize(length);
	dct<type>(&source[0], &dest[0], length);
}

template<int type>
void dct(const double *source, double *dest, int length) {
	//static_assert(type >= 1 && type <= 3, "unsupported dct type");
	if(type == 1) {
		std::vector<std::complex<double> > src(length);
		std::vector<double> dst(2*(length-1));
		for(int i=0; i<length; i++)	
			src[i] = source[i]*2;
		irfft(src, dst);
		std::copy(&dst[0], &dst[length], dest);
	} else if(type == 2) {
		std::vector<std::complex<double> > src(4*length), dst(4*length);
		for(int i=0; i<length; i++)
			src[2*i+1] = src[4*length-1-2*i] = source[i];
		fft(src, dst);	
		double scale = sqrt(1.0/length);
		dest[0] = dst[0].real()/2.0*scale;
		scale *= sqrt(2.0);
		for(int i=1; i<length; i++) 
			dest[i] = dst[i].real()/2.0*scale;
	} else if(type == 3) {
		std::vector<std::complex<double> > src(4*length), dst(4*length);
		double scale=sqrt(1.0/length);
		src[0] = source[0]/scale;
		src[2*length] = -source[0]/scale;
		scale *= sqrt(2.0);
		for(int i=1; i<length; i++) {
			src[i] = source[i]/scale;
			src[2*length-i] = -source[i]/scale;
			src[2*length+i] = -source[i]/scale;
			src[4*length-i] = source[i]/scale;
		}
		fft(src,dst);
		for(int i=0; i<length; i++)
			dest[i] = dst[2*i+1].real()/2/length;
	} 
}

template<int type>
ArrayXd idct(const ArrayXd &source) {
	ArrayXd result(source.size());
	idct<type>(&source[0], &result[0], source.size());
	return result;
}
	
template<int type> 
void idct(const std::vector<double> &source, std::vector<double> &dest) {
	int length = (int)source.size();
	dest.resize(length);
	idct<type>(&source[0], &dest[0], length);
}

template<int type> 
void idct(const double *source, double *dest, int length) {
	//static_assert(type >= 1 && type <= 3, "unsupported dct type");
	if(type==1) {
		dct<1>(source, dest, length);
		for(int i=0; i<length; i++) 
			dest[i] *= (length-1.0)/2.0;
	}
	if(type==2) dct<3>(source, dest, length);
	if(type==3) dct<2>(source, dest, length);
}

template ArrayXd dct<1>(const ArrayXd &source);
template ArrayXd dct<2>(const ArrayXd &source);
template ArrayXd dct<3>(const ArrayXd &source);
template ArrayXd idct<1>(const ArrayXd &source);
template ArrayXd idct<2>(const ArrayXd &source);
template ArrayXd idct<3>(const ArrayXd &source);

template void dct<1>(const std::vector<double> &source, std::vector<double> &dest);
template void dct<2>(const std::vector<double> &source, std::vector<double> &dest);
template void dct<3>(const std::vector<double> &source, std::vector<double> &dest);
template void idct<1>(const std::vector<double> &source, std::vector<double> &dest);
template void idct<2>(const std::vector<double> &source, std::vector<double> &dest);
template void idct<3>(const std::vector<double> &source, std::vector<double> &dest);

template void dct<1>(const double *source, double *dest, int length);
template void dct<2>(const double *source, double *dest, int length);
template void dct<3>(const double *source, double *dest, int length);
template void idct<1>(const double *source, double *dest, int length);
template void idct<2>(const double *source, double *dest, int length);
template void idct<3>(const double *source, double *dest, int length);

MatrixXd spectrogram(const ArrayXd &data, int nfft, int hopsize, const ArrayXd &window) {
	int length = ((int)data.size()-nfft)/hopsize;
	if(length<0) length=0;
	MatrixXd result(length, nfft/2);
	for(int i=0; i<length; i++) {
		ArrayXd piece(nfft); 
		memcpy(&piece[0], &data[i*hopsize], nfft * sizeof(double));
		piece *= window;
		ArrayXd spectrum = fft(piece);
		spectrum.resize(nfft/2);
		result.row(i) = spectrum;
		//if(i%1000==0) printf("%d%%\n",i*100/length);
	}
	return result;
}
