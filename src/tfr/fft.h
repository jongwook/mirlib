
#ifndef __tfr_fft_h__
#define __tfr_fft_h__

#include <complex>
#include <vector>

#include "kissfft.h"
#include "Eigen/Eigen"
using namespace Eigen;

// normal FFT
ArrayXcd fft(const ArrayXcd &source);
void fft(const std::vector<std::complex<double> > &source, std::vector<std::complex<double> > &dest);	
void fft(const std::complex<double> *source, std::complex<double> *dest, int length);
	
// FFT with real input and returning the magnitude of its fft
ArrayXd fft(const ArrayXd& source);
void fft(const std::vector<double> &source, std::vector<double> &dest);
void fft(const double *source, double *dest, int length);
	
// inverse FFT
ArrayXcd ifft(const ArrayXcd &source);
void ifft(const std::vector<std::complex<double> > &source, std::vector<std::complex<double> > &dest);
void ifft(const std::complex<double> *source, std::complex<double> *dest, int length);

// Real FFT; FFT with real input and returning only the first N/2+1 entries of the result as it is Hermitian symmetric
ArrayXcd rfft(const ArrayXd &source);
void rfft(const std::vector<double> &source, std::vector<std::complex<double> > &dest);
void rfft(const double *source, std::complex<double> *dest, int length);

// Inverse Real FFT; FFT with real input and returning only the first N/2+1 entries of the result as it is Hermitian symmetric
ArrayXd irfft(const ArrayXcd &source);
void irfft(const std::vector<std::complex<double> > &source, std::vector<double> &dest);
void irfft(const std::complex<double> *source, double *dest, int length);

// DCT with given type (1~3)
template<int type> ArrayXd dct(const ArrayXd &source);
template<int type> void dct(const std::vector<double> &source, std::vector<double> &dest);
template<int type> void dct(const double *source, double *dest, int length);
	
// IDCT with given type (1~3)
template<int type> ArrayXd idct(const ArrayXd &source);
template<int type> void idct(const std::vector<double> &source, std::vector<double> &dest);
template<int type> void idct(const double *source, double *dest, int length);

// Spectrogram
MatrixXd spectrogram(const ArrayXd &data, int nfft, int hopsize, const ArrayXd &window);

#endif
