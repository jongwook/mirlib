
#include <iostream>

#include "Audio.h"
#include "WaveReader.h"

template <typename T>
Audio<T>::Audio(const std::string &path, bool verbose /* = true */) {
	reader = NULL;
	open(path);
}

template <typename T>
Audio<T>::~Audio() {
	if(reader != NULL) {
		delete reader;
	}
}

template <typename T>
int Audio<T>::open(const std::string &path, bool verbose /* = true */) {
	// determine filetype
	size_t path_len = path.length();
	if(strncmp(path.c_str() + path_len - 4, ".wav", 4) == 0) {
		if(reader != NULL) {
			delete reader;
		}
		reader = new WaveReader<T>;
	} else {
		if(verbose)	std::cerr << "Audio format not supported\n";
		return -1;
	}
	
	int ret = reader->open(path, verbose);
	if(ret != 0) {
		return ret;
	}
	
	num_channels = reader->numChannels();
	num_frames = reader->numFrames();
	sample_rate = reader->sampleRate();
		
	for(int c=0; c<num_channels; c++) {
		audio_data[c].resize(num_frames);
	}
	reader->readSamples(audio_data);
	
	return 0;
}


template class Audio<float>;
template class Audio<double>;
template class Audio<short>;
