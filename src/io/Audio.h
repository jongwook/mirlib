
#ifndef __io_Audio_h__
#define __io_Audio_h__

#include "AudioReader.h"

template <typename T=float>
class Audio {
public:
	Audio() : reader(NULL) {}
	Audio(const std::string &path, bool verbose = true);
	~Audio();
	
	int open(const std::string &path, bool verbose = true);
	
	int sampleRate() { return sample_rate; }
	int numChannels() { return num_channels; }
	int numFrames() { return num_frames; }
	Array<T, Dynamic, 1> data() { return audio_data[0]; }
	const Array<T, Dynamic, 1>& data() const { return audio_data[0]; }
	
	inline T operator() (size_t index, int channel = 0) {
		return audio_data[channel](index);
	}
	
protected:
	AudioReader<T> *reader;
	
	int num_channels;
	int sample_rate;
	size_t num_frames;
public:
	Array<T, Dynamic, 1> audio_data[2];
};

#endif 