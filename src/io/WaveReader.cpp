
#include <iostream>
#include <vector>

#include "WaveReader.h"

template <typename T>
int WaveReader<T>::open(const std::string &path, bool verbose /* = true */) {
	int ret = AudioReader<T>::open(path, verbose);
	if(ret != 0) {
		return ret;
	}
	
	FILE *fp = this->fp;
	
	char id[4]; //four bytes to hold 'RIFF' 
	size_t size; //32 bit value to hold file size 
	short format_tag, block_align; //our 16 bit values 
	int format_length, avg_bytes_sec; //our 32 bit values 
	
	fread(id, sizeof(char), 4, fp); //read in first four bytes 
	
	if (strncmp(id, "RIFF", 4) != 0) {
		if(verbose)	std::cerr << "Unrecognizable WAV file\n";
		return -1;
	}
	
	fread(&size, 4, 1, fp); //read in 32bit size value 
	fread(id, 1, 4, fp); //read in 4 byte string now 

	//this is a wave file if it containes "WAVE" 
	if (strncmp(id, "WAVE", 4) != 0) { 
		if(verbose)	std::cerr << "Unrecognizable WAV file\n";
		return -1;
	}
	
	fread(id, 1, 4, fp); //read in 4 bytes "fmt "; 
	fread(&format_length, 4, 1,fp); 
	fread(&format_tag, 2, 1, fp); // possible format tags like ADPCM 
	fread(&this->num_channels,2,1,fp); //1 mono, 2 stereo 
	fread(&this->sample_rate, 4, 1, fp); //like 44100, 22050, etc... 
	fread(&avg_bytes_sec, 4, 1, fp); 
	fread(&block_align, 2, 1, fp); 
	fread(&this->bit_depth, 2, 1, fp); //8 bit or 16 bit file? 
	fread(id, 1, 4, fp); //read in 'data' 
	fread(&data_size, 4, 1, fp); //how many bytes of sound data we have 
	while(strncmp(id, "data", 4) != 0) {
		fseek(fp, data_size, SEEK_CUR);
		fread(id, 1, 4, fp); //read in 'data' 
		fread(&data_size, 4, 1, fp); //how many bytes of sound data we have 
		if(feof(fp)) break;
	}
	
	size_t frame_size = this->bit_depth/8 * this->num_channels;
	this->num_frames = data_size / frame_size;

	if(this->num_channels <= 0 || this->num_channels > 2 ||
	   this->sample_rate * block_align != avg_bytes_sec || block_align != this->bit_depth/8 * this->num_channels) {
		if(verbose)	std::cerr << "[WaveFile::read] Malformed WAV file\n";
		return -1;
	}
	
	// ready to read data frames
	
	return 0;
}

template <typename T>
int WaveReader<T>::readSamples(Array<T, Dynamic, 1> *audio_data) {
	FILE *fp = this->fp;
	if(fp == NULL) {
		std::cerr << "File is not opened\n";
		return -1;
	}
	
	int bit_depth = this->bit_depth;
	int num_channels = this->num_channels;
	
	size_t frames = this->num_frames;
	size_t frame_size = bit_depth/8 * num_channels;
	size_t buffer_len = frame_size * frames;
	
	std::vector<unsigned char> data(buffer_len);
	size_t read_bytes = fread(&data[0], 1, buffer_len, fp);
	size_t read_frames = read_bytes / frame_size;
	
	bool floating = is_floating<T>::value;
	
	if(bit_depth == 8) {
		if(floating) {
			for(size_t i=0; i<read_frames; i++) {
				for(int c=0; c<num_channels; c++) {
					audio_data[c](i) = (T)((data[i*num_channels + c]-128.0f)/256.0f);
				}
			}
		} else {
			for(size_t i=0; i<read_frames; i++) {
				for(int c=0; c<num_channels; c++) {
					audio_data[c](i) = (T)(data[i*num_channels + c]-128.0f);
				}
			}
		}
	} else if(bit_depth == 16) {
		short *buffer = (short *)&data[0];
		if(floating) {
			for(size_t i=0; i<read_frames; i++) {
				for(int c=0; c<num_channels; c++) {
					audio_data[c](i) = (T)(buffer[i*num_channels + c]/32768.0f);
				}
			}
		} else {
			for(size_t i=0; i<read_frames; i++) {
				for(int c=0; c<num_channels; c++) {
					audio_data[c](i) = (T)buffer[i*num_channels + c];
				}
			}
		}
	} else {
		std::cerr << "[WaveFile::read] Unrecognized bit rate\n";
		fclose(fp);
		return -1;
	}
	
	if(read_bytes < buffer_len) {
		if(feof(fp) || ferror(fp)) {
			fclose(fp);
			fp = NULL;
		}
	}
	
	// return the number of frames read
	return read_frames;

}

template class WaveReader<float>;
template class WaveReader<double>;
template class WaveReader<short>;
