
#ifndef __io_WaveReader_h__
#define __io_WaveReader_h__

#include "AudioReader.h"

template <typename T>
class WaveReader : public AudioReader<T> {	
public:
	WaveReader() : data_size(0) {}
	virtual int open(const std::string &path, bool verbose = true);
	
	int readSamples(Array<T, Dynamic, 1> *data);
	
protected:	
	size_t data_size;
};


#endif

