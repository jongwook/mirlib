
#include <iostream>

#include "AudioReader.h"

template <typename T>
AudioReader<T>::~AudioReader() {
	if(fp != NULL) {
		fclose(fp);
	}
}

template <typename T>
int AudioReader<T>::open(const std::string &path, bool verbose /* = true */) {
	fp = fopen(path.c_str(), "rb");
	if(fp == NULL) {
		if(verbose)	std::cerr << "Could not open audio file\n";
		return -1;
	}
	
	return 0;
}

template class AudioReader<float>;
template class AudioReader<double>;
template class AudioReader<short>;
