
#ifndef __io_AudioReader_h__
#define __io_AudioReader_h__

#include <cstdio>
#include <string>

#include "util/types.h"

#include "Eigen/Eigen"
using namespace Eigen;

template <typename T>
class AudioReader {
public:
	AudioReader() : fp(NULL), sample_rate(0), num_channels(0), bit_depth(0) {}
	virtual ~AudioReader();
	
	virtual int open(const std::string &path, bool verbose = true);
	bool is_open() { return fp != NULL && !ferror(fp) && !feof(fp); };
	
	int sampleRate() { return sample_rate; }
	int numChannels() { return num_channels; }
	int numFrames() { return num_frames; }
	int bitDepth() { return bit_depth; }
	
	virtual int readSamples(Array<T, Dynamic, 1> *data)=0;

protected:	
	FILE *fp;
	
	int sample_rate;
	int num_channels;
	int num_frames;
	int bit_depth;
};

#endif