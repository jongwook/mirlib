
#include "gtest/gtest.h"
#include "math/LBFGSMinimizer.h"

class MyFunction:public DifferentiableFunction {
public:
	virtual ~MyFunction() {}
	int dimension() { return 1; }
	double valueAt(double *x) { return x[0] * (x[0]-0.01); }
	void derivativeAt(double *x, double *result) { result[0] = 2*x[0]-0.01; }
};

TEST(LBFGSMinimizer, Test) {
	LBFGSMinimizer *lineSearcher = new LBFGSMinimizer(20);
	double initial[] = {0.0};
	double result[] = {0.0};
	MyFunction function;
	lineSearcher->minimize(function, initial, 1e-10, result, false);

	EXPECT_NEAR(0.005, result[0], 1e-4);
	delete lineSearcher;
}