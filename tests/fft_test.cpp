

#include <iostream>
#include <cmath>
#include <complex>

#include "gtest/gtest.h"
#include "tfr/fft.h"

using namespace std;

template<class vector_type>
void print(string label, vector_type vector);

template<class vector_type>
void print(string label, vector_type vector) {
	cout << label << " : [";
	for(int i=0; i<vector.size(); i++) {
		if(abs(vector[i])<1e-8)
			vector[i]=0.0;
		cout << vector[i] << ((i+1==vector.size())?"]":", ");
	}
	cout << endl;
}

// L1 vector_sum of a vector
template<class vector_type>
double vector_sum(vector_type vector) {
	double result = 0;
	for(int i=0; i<vector.size(); i++) {
		result += std::abs(vector[i]);
	}
	return result;
}


TEST(FFT, Test) {
	complex<double> source[6];
	source[0]=complex<double>(1,0);
	source[1]=complex<double>(4,0);
	source[2]=complex<double>(2,0);
	source[3]=complex<double>(8,0);
	source[4]=complex<double>(5,0);
	source[5]=complex<double>(7,0);
	
	ArrayXd real(6);
	real << 1, 4, 2, 8, 5, 7;
	ArrayXcd complex(6);
	complex << source[0], source[1], source[2], source[3], source[4], source[5];
	
	ArrayXcd a1 = fft(complex);
	ArrayXd a2 = fft(real);
	ArrayXcd a3 = ifft(fft(complex));
	ArrayXcd a4 = rfft(real);
	ArrayXd a5 = irfft(rfft(real));
	ArrayXd a6 = dct<1>(real);
	ArrayXd a7 = idct<1>(dct<1>(real));
	ArrayXd a8 = dct<2>(real);
	ArrayXd a9 = idct<2>(dct<2>(real));
	ArrayXd a10 = dct<3>(real);
	ArrayXd a11 = idct<3>(dct<3>(real));
	
	EXPECT_NEAR(52.422205, vector_sum(a1), 1e-4);
	EXPECT_NEAR(52.422205, vector_sum(a2), 1e-4);
	EXPECT_NEAR(27.000000, vector_sum(a3), 1e-4);
	EXPECT_NEAR(45.211103, vector_sum(a4), 1e-4);
	EXPECT_NEAR(27.000000, vector_sum(a5), 1e-4);
	EXPECT_NEAR(16.130495, vector_sum(a6), 1e-4);
	EXPECT_NEAR(27.000000, vector_sum(a7), 1e-4);
	EXPECT_NEAR(20.916234, vector_sum(a8), 1e-4);
	EXPECT_NEAR(27.000000, vector_sum(a9), 1e-4);
	EXPECT_NEAR(24.537812, vector_sum(a10), 1e-4);
	EXPECT_NEAR(27.000000, vector_sum(a11), 1e-4);
}

