#include <iostream>

using namespace std;

#include "gtest/gtest.h"
#include "Eigen/Eigen"

using namespace Eigen;

int main(int argc, char ** argv) {
	
	
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}