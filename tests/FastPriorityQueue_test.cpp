
#include "util/FastPriorityQueue.h"
#include "util/Counter.h"

#include "gtest/gtest.h"

#include <string>
using namespace std;

TEST(FastPriorityQueue, Test) {
	PriorityQueue<string> *pq = new FastPriorityQueue<string>();

	EXPECT_EQ("[]", pq->toString());
	
	pq->setPriority("one",1);
	
	EXPECT_EQ("[one : 1]", pq->toString());
	
	pq->setPriority("three",3);
	
	EXPECT_EQ("[three : 3, one : 1]", pq->toString());
	
	pq->setPriority("one", 1.1);
	
	EXPECT_EQ("[three : 3, one : 1.1, one : 1]", pq->toString());
	
	pq->setPriority("two",2);
	
	EXPECT_EQ("[three : 3, two : 2, one : 1.1, one : 1]", pq->toString());
	EXPECT_EQ("[three : 3, two : 2, ...]", pq->toString(2));

	delete pq;
}