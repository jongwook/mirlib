
#include <string>
using namespace std;

#include "gtest/gtest.h"
#include "io/Audio.h"

TEST(WaveReader, Test) {
	Audio<double> audio;
	
	string path="tests/data/sweep.wav";
	string upper="../";
	
	char *home = getenv("HOME");
	string homedir = home ? home : ".";
	string abspath = home + string("/workspace/mirlib/") + path;
	int ret = audio.open(abspath);
	if(ret == 0) {
		path = abspath;
	} else {
		for(int c=0; c<4; c++) {
			ret = audio.open(path, false);
			if(ret == 0) break;
			path = upper + path;
		}	
	}
	ASSERT_EQ(0, ret);

	EXPECT_EQ(44100, audio.sampleRate());
	EXPECT_EQ(1, audio.numChannels());
	EXPECT_EQ(44100, audio.numFrames());

	double sum = 0;
	for (int i=0; i<audio.numFrames(); i++) {
		sum += audio(i);
	}
	EXPECT_NEAR(3.5038, sum, 1e-3);
	
	Audio<short> audio2(path);

	EXPECT_EQ(44100, audio2.sampleRate());
	EXPECT_EQ(1, audio2.numChannels());
	EXPECT_EQ(44100, audio2.numFrames());
	
	double sum2 = 0;
	for (int i=0; i<audio.numFrames(); i++) {
		sum2 += audio2(i);
	}
	EXPECT_NEAR(sum, sum2/32768.0, 1e-3);
}
