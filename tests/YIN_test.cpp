
#include <string>
#include <iostream>
using namespace std;

#include "gtest/gtest.h"
#include "mir/YIN.h"
#include "mir/Chord.h"
#include "io/Audio.h"

TEST(YIN, Test) {
	Audio<double> audio;
	
	string path="tests/data/sweep.wav";
	string upper="../";
	
	char *home = getenv("HOME");
	string homedir = home ? home : ".";
	string abspath = home + string("/workspace/mirlib/") + path;
	int ret = audio.open(abspath);
	if(ret == 0) {
		path = abspath;
	} else {
		for(int c=0; c<4; c++) {
			ret = audio.open(path, false);
			if(ret == 0) break;
			path = upper + path;
		}	
	}
	ASSERT_EQ(0, ret);

	struct MyDetectedPitchHandler: public YIN::DetectedPitchHandler {
		void handleDetectedPitch(double time, double pitch) {
			times.push_back(time);
			pitches.push_back(pitch);
		}
		vector<double> times;
		vector<double> pitches;
	};

	MyDetectedPitchHandler handler;
	YIN::processAudio(audio.data(), audio.sampleRate(), handler);

	ArrayXd times = Map<ArrayXd>(&handler.times[0], handler.times.size());
	ArrayXd pitches = Map<ArrayXd>(&handler.pitches[0], handler.pitches.size()).log();

	ASSERT_TRUE(times.size() == pitches.size());
	
	int size = times.size();
	MatrixX2d X(size, 2);
	X.col(0) = VectorXd::Ones(size);
	X.col(1) = Map<VectorXd>(&times[0], size);
	VectorXd Y = Map<VectorXd>(&pitches[0], size);

	// compare the linear regression coefficients
	Vector2d coef = (X.transpose() * X).inverse() * X.transpose() * Y;
	EXPECT_NEAR(6.20256, coef[0], 1e-3);
	EXPECT_NEAR(2.30397, coef[1], 1e-3);

	auto result = YIN::transcribeAudio(audio.data(), audio.sampleRate());
	EXPECT_TRUE(result.size() > 0);
}
