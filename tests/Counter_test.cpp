
#include "gtest/gtest.h"
#include "util/Counter.h"

#include <string>
using namespace std;

TEST(Counter, Test) {
	Counter<string> *counter = new Counter<string>;
	
	EXPECT_EQ("[]", counter->toString());
	
	counter->incrementCount("planets",7);
	
	EXPECT_EQ("[planets : 7]", counter->toString());

	counter->incrementCount("planets", 1);
	
	EXPECT_EQ("[planets : 8]", counter->toString());

	counter->setCount("suns", 1);

	EXPECT_EQ("[planets : 8, suns : 1]", counter->toString());

	counter->setCount("aliens", 0);

	EXPECT_EQ("[planets : 8, suns : 1, aliens : 0]", counter->toString());
	EXPECT_EQ("[planets : 8, suns : 1, ...]", counter->toString(2));

	EXPECT_EQ(9, counter->totalCount());

	delete counter;
}

